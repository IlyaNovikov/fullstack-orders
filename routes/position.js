const express = require('express');
const controller = require('../controllers/position');
const passport = require("passport");
const router = express.Router();

router.get('/:categoryId',  passport.authenticate('jwt', {session: false}), controller.getCategoryId);
router.patch('/:id',  passport.authenticate('jwt', {session: false}), controller.update);
router.delete('/:id',  passport.authenticate('jwt', {session: false}), controller.remove);

router.post('/', controller.create);

module.exports = router;
