const admin = require("firebase-admin");
const serviceAccount = require("./config/fullstack-46c1a-firebase-adminsdk-q4zc9-14702357c7.json");

const db = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});
// const firebase = require("firebase");
// const fireBaseConfig = require('./config/keys')
// const db = firebase.initializeApp(fireBaseConfig);

module.exports = db;
