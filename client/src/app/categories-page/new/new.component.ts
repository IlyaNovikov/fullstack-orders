import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {CategoryService} from "../../shared/services/category.service";
import {switchMap} from "rxjs/operators";
import {of, Subscription} from "rxjs";
import {MaterialService} from "../../shared/classes/material.service";
import {Category, Message} from "../../shared/interfaces/interfaces";

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.less']
})
export class NewComponent implements OnInit, OnDestroy {
  isNew = true;
  form: FormGroup;
  image: File;
  imagePreview: string | ArrayBuffer;
  aSub: Subscription;
  category: Category;
  @ViewChild('input') inputRef: ElementRef

  constructor(private route: ActivatedRoute,
              private categoryService: CategoryService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
        name: new FormControl(null, [Validators.required])
      }
    )

    this.form.disable();

    this.aSub = this.route.params
      .pipe(
        switchMap((params: Params) => {
          if (params['id']) {
            this.isNew = false;
            return this.categoryService.getById(params['id']);
          }
          return of(null);
        })
      )
      .subscribe(
        (category: Category) => {
          if (category) {
            this.category = category;
            this.form.patchValue({
              name: category.name
            })
            this.imagePreview = category.imageSrc
            MaterialService.updateTexpInputs();
          }
          this.form.enable()
        },
        error => {
          MaterialService.toast(error.error.message);
        })
    // this.route.params.subscribe((params: Params) => {
    //   if (params['id']) {
    //     this.isNew = false;
    //   }
    // })

  }

  ngOnDestroy() {
    this.aSub.unsubscribe();
  }

  onSubmit() {
    let obs$;
    this.form.disable();

    if (this.isNew) {
      obs$ = this.categoryService.create(this.form.value.name, this.image)
    } else {
      obs$ = this.categoryService.update(this.category._id, this.form.value.name, this.image)
    }
    obs$.subscribe(
      (category: Category) => {
        this.category = category;
        MaterialService.toast('Категория сохранена');
        this.form.enable()
      },
      error => {
        MaterialService.toast(error.error.message);
        this.form.enable()
      }
    )
  }

  triggerClick() {
    this.inputRef.nativeElement.click();
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.image = file;

    const reader = new FileReader();

    reader.onload = () => {
      this.imagePreview = reader.result;
    }
    reader.readAsDataURL(file)
  }

  deleteCategory() {
    const description = `Вы хотите удалить категорию "${this.category.name}"?`;
    const confirm = window.confirm(description);

    if (confirm) {
      this.categoryService.removeCategory(this.category._id)
        .subscribe(
          (message: Message) => {MaterialService.toast(message.message);},
          (error) => {MaterialService.toast(error.error.message);},
          () => {this.router.navigate(['/categories'])},
        );
    }


  }
}
