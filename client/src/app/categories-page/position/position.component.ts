import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PositionsService} from "../../shared/services/positions.service";
import {Subscription} from "rxjs";
import {Message, Position} from "../../shared/interfaces/interfaces";
import {MaterialInstance, MaterialService} from "../../shared/classes/material.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.less']
})
export class PositionComponent implements OnInit, OnDestroy, AfterViewInit {
  aSub: Subscription;
  positions: Position[];
  loading = false;
  modal: MaterialInstance;
  form: FormGroup;
  positionId: null | string;

  @ViewChild('modal') modalRef: ElementRef;
  @Input('categoryId') categoryId: string;

  constructor(private positionService: PositionsService) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.aSub = this.positionService.fetch(this.categoryId)
      .subscribe(
        (positions: Position[]) => {
          this.loading = false;
          this.positions = positions;
        }
      )
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      cost: new FormControl(1, [
        Validators.required,
        Validators.min(1),
      ])
    })
  }

  ngOnDestroy() {
    this.aSub.unsubscribe();
    this.modal.destroy()
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef)
  }

  onSelectPosition(position: Position) {
    this.positionId = position._id;
    this.form.patchValue({
      name: position.name,
      cost: position.cost,
    })
    this.modal.open();
    MaterialService.updateTexpInputs();
  }

  onAddPosition() {
    this.positionId = null;
    this.modal.open();
    this.form.patchValue({
      name: '',
      cost: 1,
    })
    MaterialService.updateTexpInputs();
  }

  onCancel() {
    this.modal.close()
  }

  onSubmit() {
    this.form.disable();
    const newPosition: Position = {
      name: this.form.value.name,
      cost: this.form.value.cost,
      category: this.categoryId,
    };
    if(this.positionId) {
      newPosition._id = this.positionId;
      this.positionService.update(newPosition)
        .subscribe(
          (pos: Position) => {
            const idx = this.positions.findIndex(p => p._id === pos._id);
            this.positions[idx] = pos;
            MaterialService.toast('Изменения сохранены');
          },
          error => {
            MaterialService.toast(error.error.message);
          },
          () => {
            this.modal.close();
            this.form.enable();
            this.form.reset({name: '', cost: 1});
          }
        )
    } else {
      this.positionService.create(newPosition)
        .subscribe((pos: Position) => {
            MaterialService.toast('Позиция добавлена');
            this.positions.push(pos);
          },
          error => {
            MaterialService.toast(error.error.message);
          },
          () => {
            this.modal.close();
            this.form.enable();
            this.form.reset({name: '', cost: 1});
          });
    }

  }

  onDeletePosition(event:Event, position: Position) {
    event.stopPropagation();
      const confirm = window.confirm(`Вы хотите удалить позицию ${position.name}`);
      if (confirm) {
        this.positionService.deletePosition(position)
          .subscribe(
            (message: Message) => {
              const idx = this.positions.findIndex(p => p._id === position._id);
              this.positions.splice(idx, 1);
              MaterialService.toast(message.message);
            },
            error => {
              MaterialService.toast(error.error.message);
            },
          )
      }
  }
}
