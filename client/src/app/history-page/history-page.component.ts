import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MaterialInstance, MaterialService} from "../shared/classes/material.service";

import {OrdersService} from "../shared/services/orders.service";
import {Filter, Order} from "../shared/interfaces/interfaces";
import {Subscription} from "rxjs";

const STEP = 2;

@Component({
  selector: 'app-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.less']
})
export class HistoryPageComponent implements OnInit, OnDestroy, AfterViewInit {
  isFilterVisible = false;
  tooltip: MaterialInstance;
  offset = 0;
  limit = STEP;
  aSub: Subscription;
  orders: Order[] = [];
  loading = false;
  reloading = false;
  noMoreOrders = false;
  filter: Filter = {};

  @ViewChild('tooltip') tooltipRef: ElementRef;

  constructor(private ordersService: OrdersService) {
  }

  ngOnInit(): void {
    this.reloading = true;
    this.fetch();
  }

  private fetch() {
    const params = Object.assign({}, this.filter, {
      offset: this.offset,
      limit: this.limit,
    })
    this.aSub = this.ordersService.fetch(params).subscribe(
      (orders: Order[]) => {
        this.noMoreOrders = orders.length < STEP;
        this.orders = this.orders.concat(orders);
        this.loading = false;
        this.reloading = false;
      }
    )
  }

  ngAfterViewInit() {
    this.tooltip = MaterialService.initTooltip(this.tooltipRef);
  }

  ngOnDestroy() {
    this.tooltip.destroy();
    this.aSub.unsubscribe();
  }

  loadMoreOrders() {
    this.loading = true;
    this.offset += STEP;
    this.fetch();
  }

  applyFilter(filter: Filter) {
    this.orders =[];
    this.offset = 0;
    this.filter = filter;
    this.reloading = true;
    this.fetch();
  }
  isFiltered(): boolean {
    return Object.keys(this.filter).length !==0;
  }
}
