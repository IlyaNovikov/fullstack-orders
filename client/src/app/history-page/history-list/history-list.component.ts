import {AfterViewInit, Component, ElementRef, Input, OnDestroy, ViewChild} from '@angular/core';
import {Order} from "../../shared/interfaces/interfaces";
import {MaterialInstance, MaterialService} from "../../shared/classes/material.service";

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.less']
})
export class HistoryListComponent implements OnDestroy, AfterViewInit {

  @Input('orders') orders: Order[];
  @ViewChild('modal') modalRef: ElementRef;

  modal: MaterialInstance;
  selectedOrder: Order;

  constructor() {
  }


  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  ngOnDestroy() {
    this.modal.destroy();
  }

  computePrice(order: Order) {
    return order.list.reduce((total, item) => {
      return total += item.quantity * item.cost;
    }, 0)
  }

  selectModal(order: Order) {
    this.selectedOrder = order;
    this.modal.open();
  }

  closeModal() {
    this.modal.close();
  }
}
