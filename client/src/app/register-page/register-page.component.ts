import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../shared/services/auth.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {MaterialService} from "../shared/classes/material.service";

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.less']
})
export class RegisterPageComponent implements OnInit, OnDestroy {
  myForm: FormGroup
  aSub: Subscription;
  constructor(private auth: AuthService, private  router: Router) { }

  ngOnInit(): void {
    this.myForm = new FormGroup({
      email: new FormControl(null, [
        Validators.email,
        Validators.required
      ]),
      password: new FormControl(null, [
        Validators.minLength(8),
        Validators.required
      ])
    });
  }
  ngOnDestroy() {
    if(this.aSub){
      this.aSub.unsubscribe();
    }

  }

  onSubmit() {
    this.myForm.disable()
    this.aSub = this.auth.register(this.myForm.value).subscribe(
      () =>{
        this.router.navigate(['/login'], {queryParams: {
          registered: true,
          }});
      }, error => {
        MaterialService.toast(error.error.message)
        console.log(error);
        this.myForm.enable();
      }
    )
  }
}
