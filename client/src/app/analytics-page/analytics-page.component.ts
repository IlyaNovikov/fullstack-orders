import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AnalyticsService} from "../shared/services/analytics.service";
import {Subscription} from "rxjs";
import {Chart, registerables} from "chart.js";
import {AnalyticsPage} from "../shared/interfaces/interfaces";

@Component({
  selector: 'app-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.less']
})
export class AnalyticsPageComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild('gain') gainRef: ElementRef;
  @ViewChild('order') orderRef: ElementRef;
  average: number;
  pending = true;
  aSub: Subscription;

  constructor(private service: AnalyticsService) {
  }

  ngOnInit() {
    Chart.register(...registerables);
  }

  ngAfterViewInit() {

    const gainConfig: any = {
      label: 'Выручка',
      color: 'rgb(255,99,132)',
    };

    const orderConfig: any = {
      label: 'Заказы',
      color: 'rgb(132,99,255)',
    };

    this.aSub = this.service.getAnalytics()
      .subscribe((data: AnalyticsPage) => {
        this.average = data.average;
        const labelsGain = [];
        const dataSetGain = [];
        const orderSetGain = [];
        for (let chartKey in data.chart) {
          labelsGain.push(data.chart[chartKey].label);
        }
        for (let chartKey in data.chart) {
          dataSetGain.push(data.chart[chartKey].gain);
          orderSetGain.push(data.chart[chartKey].order);
        }

        const gainCtx = this.gainRef.nativeElement.getContext('2d');
        const orderCtx = this.orderRef.nativeElement.getContext('2d');
        gainCtx.canvas.height = '300px';
        orderCtx.canvas.height = '300px';

        new Chart(gainCtx, {
            type: 'line',
            options: {
              responsive: true
            },
            data: {
              labels: labelsGain,
              datasets: [
                {
                  label: gainConfig.label,
                  data: dataSetGain,
                  borderColor: gainConfig.color,
                  fill: false
                }
              ]
            }
          });

        new Chart(orderCtx, {
          type: 'line',
          options: {
            responsive: true
          },
          data: {
            labels: labelsGain,
            datasets: [
              {
                label: orderConfig.label,
                data: orderSetGain,
                borderColor: orderConfig.color,
                fill: false
              }
            ]
          }
        });

        this.pending = false;
      })
  }

  ngOnDestroy() {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

}

function createChartConfig({labels, data, label, color}) {
  return {
    type: 'line',
    options: {
      responsive: true
    },
    data: {
      labels,
      datasets: [
        {
          label, data,
          borderColor: color,
          fill: false
        }
      ]
    }
  }
}
