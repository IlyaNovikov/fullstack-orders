import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {MaterialInstance, MaterialService} from "../shared/classes/material.service";
import {OrderService} from "./order.service";
import {Order, OrderPosition} from "../shared/interfaces/interfaces";
import {OrdersService} from "../shared/services/orders.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.less'],
  providers: [OrderService],
})
export class OrderPageComponent implements OnInit, AfterViewInit, OnDestroy {

  isRoot: boolean;
  modal: MaterialInstance;
  pending = false;
  aSub: Subscription

  @ViewChild('modal') modalRef: ElementRef;

  constructor(private router: Router,
              public orderService: OrderService,
              private ordersSc: OrdersService) {
  }

  ngOnInit(): void {
    this.isRoot = this.router.url === '/order';
    this.router.events.subscribe(event => {    // Прослушка событий роута
      if (event instanceof NavigationEnd) {         // Если переход окончен
        this.isRoot = this.router.url === '/order';
      }
    })
  }

  ngOnDestroy() {
    this.modal.destroy();
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  openModal() {
    this.modal.open();
  }

  closeModal() {
    this.modal.close();
  }

  removePosition(orderPosition: OrderPosition) {
    this.orderService.remove(orderPosition);
    MaterialService.toast(`Товар "${orderPosition.name}" был удален`);
  }

  submit() {
    this.pending = true;
    const order: Order = {
      list: this.orderService.list.map(item => {
        delete item._id;
        return item;
      }),
    }

    this.aSub = this.ordersSc.create(order).subscribe(
      (newOrder) => {
        MaterialService.toast(`Заказ №${newOrder.order} был добавлен`);
        this.orderService.clear();
      }, error => {
        MaterialService.toast(error.error.message);
      }, () => {
        this.modal.close();
        this.pending = false;
      }
    )

  }
}
